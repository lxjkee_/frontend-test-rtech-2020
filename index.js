const cors = require("cors");
const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const data = require("./data");
const app = express();

app.use(express.static(path.join(__dirname, "/public")));
app.use(bodyParser.json());
app.use(cors());
app.options("*", cors());

const users = generateUsers(50);

app.get("/api/list", (req, res) => {
  return res.json(users);
});

app.get("/api/delete/:id", (req, res) => {
  let user = null;
  if (
    req.params.id === undefined ||
    (user = users.findIndex((u) => u.id.toString() === req.params.id.toString())) === -1
  )
    return res.writeHead(404).end();
  return res.json(users.splice(user, 1)[0]);
});

app.post("/api/edit/:id", (req, res) => {
  if (!req.body.name || !req.body.phone) return res.writeHead(400).end();
  let user = null;
  if (
    req.params.id === undefined ||
    !(user = users.find((u) => u.id.toString() === req.params.id.toString()))
  )
    return res.writeHead(404).end();
  user.name = req.body.name || user.name;
  user.surname = req.body.surname || "";
  user.middlename = req.body.middlename || "";
  user.phone = req.body.phone || user.phone;
  user.address = req.body.address || "";
  return res.json(user);
});

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "/public/index.html"));
});

app.listen(8080, () => {
  console.log("Server running at 8080");
});

function generateUsers(amount) {
  const result = [];
  for (let i = 0; i < amount; i++) {
    const user = {
      id: i,
      name: data.names[Math.round(Math.random() * (data.names.length - 1))],
      surname: data.names[Math.round(Math.random() * (data.names.length - 1))] + "ов",
      middlename:
        data.names[Math.round(Math.random() * (data.names.length - 1))] + "ович",
      phone: generatePhone(),
      address: `Россия, г. ${
        data.cities[Math.round(Math.random() * (data.cities.length - 1))]
      }, ул ${
        data.streets[Math.round(Math.random() * (data.streets.length - 1))]
      }, ${Math.round(Math.random() * 99)}`,
    };
    result.push(user);
  }
  return result;
}

function generatePhone() {
  let region = Math.round(Math.random() * 899) + 100;
  let city = Math.round(Math.random() * 899) + 100;
  let xx = Math.round(Math.random() * 89) + 10;
  let yy = Math.round(Math.random() * 89) + 10;
  return `+7 (${region}) ${city}-${xx}-${yy}`;
}
